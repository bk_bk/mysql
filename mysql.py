# -*- coding: utf-8 -*-
import MySQLdb
#linux
conn=MySQLdb.connect(host="127.0.0.1",port=3307,user="root",passwd="usbw",db="test2")
#获取数据库游标
t = conn.cursor()
t.execute("INSERT INTO student(name,sex,age) SELECT name,sex,age FROM student WHERE id=10")

#create table 新建表名称（ID列，name列……）
#ID列设置： int unsigned是int类型 not null是不为空 auto_increment是自增 primary key是定义为KEY
#t.execute("create table student(id int unsigned not null auto_increment primary key,name char(8) not null,sex char(4) not null,age tinyint unsigned not null)")

#INSERT INTO 要插入数据的表名（要插入数据列：name，sex……）VALUES（'name的数据'，'sex的数据'……）
#因为设置了ID自增所以可以不用管ID
#t.execute("INSERT INTO student(name, sex, age) VALUES('qong','f','16')")

#select *（可以为ID等其他列，*表示全部）from 查找的表名称
#从头查找这个表的其中一行数据，*换成ID的话只显示ID这一列
#查找完一行后会自动跳到下一行
'''
t.execute("select * from student")
a=t.fetchone()
print a
#查看2条信息,游标定在本次查询的最后一条数据
a=t.fetchmany(2)
print a
a=t.fetchone()
print a
#查看剩下的全部
a=t.fetchall()
print "isdjif",a
#控制浮标的位置0为第一行
#relative 当前行移动，absolute从头移动
t.scroll(0,'absolute')
#返回附表所指的这样的数据
a=t.fetchone()
print a
'''

#可以这样重复添加数据,更新数据，用循环可以多条添加
'''
ff=[('jing','u','19'),('qwnn','hh','91')]
dd=5
sql="INSERT INTO student(name, sex, age) VALUES('%s','%s','%s')"%ff[0]
sql2="UPDATE student set name='uuy',age='66' WHERE id='%d'"%dd
print(sql)
print(sql2)
t.execute(sql2)
'''

#删除数据
#delete from 表名 where id=2（条件）
#sql="delete from students where id=2"
#t.exeucte(sql)

#列的操作---------------------------------------------

#表中末尾添加一列
#alter table 表明 add 要插入的列名 数据类型
#alter table students add address char(60)
#某列后面插入一列
#alter table 表名 add 要插入的列名 数据类型 after 被插入的列名
#alter table students add birthday date after age

#修改列
#alter table 表名 change 列名称 列新名称 新数据类型
#alter table students change tel telphone char(13)
#更新列的数据类型
#alter table 表名 change 列名称 列名称 新数据类型
#alter table students change name name char(16) not null
#删除一列
#alter table 表名 drop 列名称
#alter table students drop birthday

#表的操作
#重命名表
#alter table 表名 rename 新表名
#删除表
#drop table 表名

#关闭数据库游标
t.close()
#提交操作
conn.commit()
#关闭数据库
conn.close()
